"""
Make sure that the executable for prank is in the path

Write peptide alignment for cary clades
back translate to get cds alignment

Also write corresponding rooted tree files and check for seqids
"""
import sys,os,newick3,phylo3
from Bio import SeqIO

def get_name(label):
	return label[:4]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: python habit_contrast_codon_prepare_alignments.py pep_fasta cds_fasta cary_clade_dir out_dir"
		sys.exit()
	
	all_pep_fasta = sys.argv[1]
	cary_cds_fasta = sys.argv[2]
	treDIR = sys.argv[3]+"/"
	outDIR = sys.argv[4]+"/"

	#read in tree and fix name formats
	for i in os.listdir(treDIR):
		if i[-5:] != ".cary": continue
		with open(treDIR+i,"r")as infile:
			intree = newick3.parse(infile.readline())
		curroot = intree
		leaves = curroot.leaves()
		for leaf in leaves:
			label = leaf.label
			label = label.replace("-","_") #fix some old formatting issue
			if get_name(label) == "BVGI":
				label = label.split("_")[0]
				#BVGI was translated using FrameDP and formated differently
			leaf.label = label
		#labels = get_front_labels(curroot)
		#if len(labels) != len(set(labels)):
			#print i #checked whether this creates duplicated tabels
			#no duplicated labels created
		with open(outDIR+i+".namefixed","w") as outfile:
			outfile.write(newick3.tostring(curroot)+";")

	print "Reading the peptide fasta file"
	pepDICT = {} #hash table of taxonID -> seqID -> seq
	with open(all_pep_fasta,"rU") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			seqID,seq = str(record.id),str(record.seq)
			if "@" in seqID: #only look at Caryophyllales taxa
				taxonID = get_name(seqID)
				if taxonID == "BVGI":
					seqID = seqID.split("_")[0]
					#BVGI was translated using FrameDP and formated differently
				if taxonID not in pepDICT:
					pepDICT[taxonID] = {} #key is seqID, value is seq
					print "Adding sequence from",taxonID
				pepDICT[taxonID][seqID] = seq[:seq.find("*")]#remove stop codons

	#write out the peptide fasta file
	for i in os.listdir(outDIR):
		if ".namefixed" not in i: continue
		with open(outDIR+i,"r")as infile:
			intree = newick3.parse(infile.readline())
		seqIDs = get_front_labels(intree)
		with open(outDIR+i.replace(".namefixed",".pep.fa"),"w") as outfile:
			for seqID in seqIDs:
				taxonID = get_name(seqID)
				seq = pepDICT[taxonID][seqID]
				seq = seq.replace("U","X")
				seq = seq.replace("u","x")
				outfile.write(">"+seqID+"\n"+seq+"\n")
				
	#align the peptides with prank using the rooted cary clade as the guide tree
	#only do one iteration here. prank do not improve much and tend to push out
	#blocks with no good reason with additional iterations
	for i in os.listdir(outDIR):
		if i[-7:] == ".pep.fa":
			name = outDIR+i.replace(".pep.fa","")
			cmd = "prank -d="+outDIR+i+" "+"-t="+name+".namefixed -once -o="+name+" -protein"
			print cmd
			os.system(cmd)
			os.system("mv "+name+".best.fas "+name+".pep.prank.aln")

	print "Reading the cds fasta file"
	cdsDICT = {} #hash table of taxonID -> seqID -> seq
	with open(cary_cds_fasta,"rU") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			seqid,seq = str(record.id),str(record.seq)
			if "@" in seqid: #only look at Caryophyllales taxa
				taxonID = get_name(seqid)
				if taxonID not in cdsDICT:
					cdsDICT[taxonID] = {} #key is seqid, value is seq
					print "Adding sequence from",taxonID
				cdsDICT[taxonID][seqid] = seq

	for i in os.listdir(outDIR):
		if i[-13:] != "pep.prank.aln": continue
		name = outDIR+i.replace(".pep.prank.aln","")
		seqids = []
		with open(outDIR+i,"r") as infile:
			for line in infile:
				if line[0] == ">":
					seqids.append((line.strip())[1:])
		#write out the cds fasta file with exactly the same seqids and in the same order
		#as the peptide alignment
		with open(name+".cds.fa","w") as outfile:
			for seqid in seqids:
				taxonid = get_name(seqid)
				seq = cdsDICT[taxonid][seqid]
				outfile.write(">"+seqid+"\n"+seq+"\n")
		#back translate using pal2nal
		cmd = "pal2nal.pl "+outDIR+i+" "+name+".cds.fa -output fasta >"+name+".pal2nal"
		print cmd
		os.system(cmd)
		
