"""
.rawblastn file columns:
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length 
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

sequences that are from genome annotation do not have "@" in their ID

Input: nuceotide fasta files. Sequences from transcriptome have "@" in them
	Sequences from genome annotation do not have a "@" in sequence ids

Output is a fasta file ends with "_R" with all the directions fixed
"""

import os,sys
import networkx as nx
from Bio import SeqIO

PERCENT_IDENT = 0.4


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python flip_dna_direction.py fastaDIR num_cores >logfile"
		sys.exit(0)

	DIR = sys.argv[1]+"/"
	num_cores = sys.argv[2]
	for i in os.listdir(DIR):
		if i[-3:] != ".fa" or "_R" in i:
			#Fasta files with _R are output files. Skip these
			continue
		print i

		print "conducting blast on "+DIR+i
		#I tried reading in the all-by-all blast results and sort through it
		#It is actually much more efficient to do the blast again for each cluster
		#create blast database
		cmd = "makeblastdb -in "+DIR+i+" -parse_seqids -dbtype nucl -out "+DIR+i+".db"
		print cmd
		os.system(cmd)
		#all-by-all blast
		cmd = "blastn -db "+DIR+i+".db -query "+DIR+i
		cmd += " -evalue 10 -num_threads "+num_cores+" -max_target_seqs 10000 -out "+DIR+i+".rawblastn "
		cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)
		print "blast is done"
		
		print "Filtering edges"
		infile = open(DIR+i+".rawblastn","r")
		outfile = open(DIR+i+".edges","w")
		for line in infile:
			if len(line) > 3:
				spls = (line.strip()).split("\t")
				query,hit = spls[0],spls[2]
				if query != hit and float(spls[6])/float(spls[1])>PERCENT_IDENT:
					if int(spls[13])-int(spls[12]) > 0: direction = "+"
					else: direction = "-"
					outfile.write(query+"\t"+hit+"\t"+direction+"\n")
		infile.close()
		outfile.close()

		print "Reading the graph from",DIR+i+".edges"
		infile = open(DIR+i+".edges","r")
		G = nx.Graph()
		start = None #find a model seq to start with. Use the last one
		#if no model seq is in the cluster
		for line in infile:
			if len(line) > 3:
				spls = line.strip().split("\t")
				query,hit,direction = spls[0],spls[1],spls[2]
				if start == None and "@" not in query:
					start = query #set the start for traversal
				G.add_edge(query,hit,direction=direction)
		if start == None: start = query
		infile.close()
		print "Starting traversal from",start

		#Breadth first traversal and label the direction
		edges = nx.bfs_edges(G,start)
		first = True #mark the start
		for edge in edges:
			node1,node2,direction = edge[0],edge[1],G[edge[0]][edge[1]]["direction"]
			#print node1,node2,direction
			if first == True: #set the start point to keep the direction
				G.node[node1]["keep"] = True
				first = False
			if G.node[node1]["keep"]:
				if direction == "+": G.node[node2]["keep"] = True
				else: G.node[node2]["keep"] = False
			else:
				if direction == "+": G.node[node2]["keep"] = False
				else: G.node[node2]["keep"] = True
		
		#Iterate over all edges to look for conflicts
		for edge in G.edges():
			node1,node2,direction = edge[0],edge[1],G[edge[0]][edge[1]]["direction"]
			try:
				keep1,keep2 = G.node[node1]["keep"],G.node[node2]["keep"]
				if keep1 == keep2 and direction == "+": continue
				if keep1 != keep2 and direction == "-": continue
				print "Conflict found in file",i
				break
			except: pass
		
		#print the updated fasta file with direction fixed
		handle = open(DIR+i,"r")
		outfile = open(DIR+i.replace(".fa","_R.fa"),"w")
		for seq_record in SeqIO.parse(handle,"fasta"):
			seqid = str(seq_record.id)
			seq = seq_record.seq
			try:
				if G.node[seqid]["keep"] == False:
					seqid = "_R_"+seqid
					seq = seq.reverse_complement()
				outfile.write(">"+seqid+"\n"+str(seq)+"\n")
			except: pass
		handle.close()
		outfile.close()
		
