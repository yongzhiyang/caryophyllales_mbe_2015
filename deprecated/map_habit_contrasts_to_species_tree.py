"""
go through rooted gene trees and map bipartition support on species tree
Still has the problem of overestimating support
May use triplet support instead in the future
"""
contrast1 = ["CGGO","WOBD"] #Plumbago_auriculata, Limonium_spectabile, both Plumbaginaceae

contrast2 = ["CPKPH","JLOV","BLWH","CPLT","EZGR","GCYL","IWIS","KDCH","LLQV","UQCB"]
#Lophophora_williamsii,Pereskia_aculeata;Cactaceae
#Portulaca_mauii,Portulaca_grandiflora,Portulaca_oleracea,Portulaca_suffruticosa,Portulaca_pilosa
#Portulaca_umbraticola,Portulaca_cryptopetala,Portulaca_molokaiensis;Portulacaceae

contrast3 = ["BKQU","MRKX"]
#Phytolacca_americana,Phytolacca_bogotensis;Phytolaccaceae

contrast4 = ["ILU5","AZBL","ILU2","SFKQ"]
#Phytolaccaceae

contrast5 = ["ILU6","JAFJ","JGAB","HMFE","EGOS","ZBTA","VJPU","ILU1"]
#Nyctaginaceae


import phylo3,newick3,os,sys,math

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_back_labels(node,root):
	all_labels = get_front_labels(root)
	front_labels = get_front_labels(node)
	return set(all_labels) - set(front_labels) #labels do not repeat
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def get_back_names(node,root): #may include duplicates
	back_labels = get_back_labels(node,root)
	return [get_name(i) for i in back_labels]

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python map_habit_contrasts_to_species_tree.py out_info sptree log"
		sys.exit(0)
	
	distDICT = {} #key is node, value is distribution of contrasts on this node
	countDICT = {} #key is node, value is number of contrasts on this node
	with open(sys.argv[2],"r") as infile:
		sptree = newick3.parse(infile.readline())
	for spnode in sptree.iternodes():
		if not spnode.istip:
			distDICT[spnode] = ""
			countDICT[spnode] = 0
	contrast1_node = phylo3.getMRCA(contrast1,sptree)
	contrast2_node = phylo3.getMRCA(contrast2,sptree)
	contrast3_node = phylo3.getMRCA(contrast3,sptree)
	contrast4_node = phylo3.getMRCA(contrast4,sptree)
	contrast5_node = phylo3.getMRCA(contrast5,sptree)
	
	infile = open(sys.argv[1],"r")
	first = True
	line_count = 0
	for line in infile:
		if first == True:
			first = False
			continue
		if "habit" not in line: continue #only look at habit contrast for now
		line_count += 1
		spls = line.strip().split("\t")
		homologID,contrast,bipart_string = spls[1],float(spls[2]),spls[3]
		genfront_labels = ((bipart_string.split("),(")[0]).replace("(","")).split(",")
		genfront_names = [get_name(i) for i in genfront_labels]
		spnode = phylo3.getMRCA(genfront_names,sptree)
		distDICT[spnode] += line
		countDICT[spnode] += 1
		if line_count % 10 == 0:
			print line_count,"contrasts count"
	infile.close()
	
	for spnode in sptree.iternodes():
		if not spnode.istip:
			spnode.label = countDICT[spnode]
	with open(sys.argv[2]+".hab_contrast_count","w") as outfile:
		outfile.write(newick3.tostring(sptree)+";\n")
	with open(sys.argv[1]+".contrast1","w") as outfile:
		outfile.write(distDICT[contrast1_node])
	with open(sys.argv[1]+".contrast2","w") as outfile:
		outfile.write(distDICT[contrast2_node])
	with open(sys.argv[1]+".contrast3","w") as outfile:
		outfile.write(distDICT[contrast3_node])
	with open(sys.argv[1]+".contrast4","w") as outfile:
		outfile.write(distDICT[contrast4_node])
	with open(sys.argv[1]+".contrast5","w") as outfile:
		outfile.write(distDICT[contrast5_node])
