"""
prepare a taxon name table in which each line look like:
GJNX	Aizoaceae_Cypselea_humifusum_H

input trees are the cary clades with name fixed
input alignment are the output of pal2nal

label and unrooot the input rooted cary clades
run codeml
summarize codemel output
"""
import newick3,phylo3,os,sys
import collections

taxaA = ["ILU6","JAFJ","JGAB","HMFE","EGOS","ZBTA","VJPU","ILU1"]#Nyctaginaceae
taxaB = ["ILU5","AZBL","ILU2","SFKQ"]#Phytolaccaceae
taxaC = ["BKQU","MRKX"]#Phytolacca_americana,Phytolacca_bogotensis;Phytolaccaceae
taxaD = ["CPKP","JLOV","BLWH","CPLT","EZGR","GCYL","IWIS","KDCH","LLQV","UQCB"]
#Lophophora_williamsii,Pereskia_aculeata;Cactaceae
#Portulaca_mauii,Portulaca_grandiflora,Portulaca_oleracea,Portulaca_suffruticosa,Portulaca_pilosa
#Portulaca_umbraticola,Portulaca_cryptopetala,Portulaca_molokaiensis;Portulacaceae
taxaE = ["CGGO","WOBD"] #Plumbago_auriculata, Limonium_spectabile, both Plumbaginaceae
HABITS = ["H","W"]

TAXA = taxaC

def get_name(label):
	return label[:4]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

#remove branch length and unroot a newick
def remove_branch_length(string):
	newstring = ""
	add_to_newstring = True
	for character in string:
		if character == ":":
			add_to_newstring = False
		elif character == ")" or character == ";" or character == ",":
			add_to_newstring = True
		if add_to_newstring:
			newstring += character
	return newstring
	
#return "W" if all tips are woody, and return "H" if all are herbaceous
#return None otherwise
def get_habit(node,habitDICT):
	names = get_front_names(node)
	habits = []
	for name in names:
		habits.append(habitDICT[name])
	if len(set(habits)) == 1:
		return habits[0]
	else: return None

def run_codeml(treefile,alnfile,output):
	ctl = treefile+".codeml.ctl"
	with open(ctl,"w") as outfile:
		outfile.write("seqfile = "+alnfile+"\n")
		outfile.write("outfile = "+output+"\n")
		outfile.write("treefile = "+treefile+"\n")
		outfile.write("verbose = 2\nCodonFreq = 2\ncleandata = 0\nNSsites = 0\nfix_omega = 0\n")
		outfile.write("clock = 0\nncatG = 10\nrunmode = 0\nfix_kappa = 0\nfix_alpha = 1\nSmall_Diff = 5e-07\nRateAncestor = 0\n")
		outfile.write("icode = 0\nalpha = 0\nseqtype = 1\nomega = 0.2\ngetSE = 0\nnoisy = 9\nkappa = 2.5\nmodel = 2\nverbose = 1\n")
	os.system("./codeml "+ctl)
		
if __name__ =="__main__":
	if len(sys.argv) != 4:
		print "usage: python habit_contrast_codon.py treDIR codon_alnDIR taxon_name_subst_table"
		sys.exit()
	
	treDIR = sys.argv[1]+"/"
	alnDIR = sys.argv[2]+"/"
	taxon_table = sys.argv[3]
	contrastname = "taxaC"
	
	#read in the habit for the ingroups
	habitDICT = {} #key is name, value is "H" or "W"
	infile = open(taxon_table,"r")
	for line in infile:
		name = line.split("\t")[0]
		habit = (line.strip()).split("_")[-1]
		if habit in HABITS:
			habitDICT[name] = habit
	print "Habit information for",len(habitDICT),"taxa read"

	for i in os.listdir(treDIR):
		if i[-10:] != ".namefixed": continue
		cladeID = i.replace(".namefixed","")
		print cladeID
		with open(treDIR+i,"r")as infile:
			root = newick3.parse(infile.readline())
		if root.nchildren != 2:
			print "Check rooting. Root should be bifurcating"
			sys.exit()
		count = 0 #keep track of number of contrasts from each cary clade
		labeled = [] #keep track of seqids that's already been labeled
		going = True
		while going:
			going = False
			with open(treDIR+i,"r")as infile: #readin a rooted cladogram
				root = newick3.parse(remove_branch_length(infile.readline()))
			for node in root.iternodes(order=1):#POSTORDER,tip to root
				if node.istip: continue #only look at internal nodes
				child0,child1 = node.children[0],node.children[1]
				names0,names1 = get_front_names(child0),get_front_names(child1)
				habit0,habit1 = get_habit(child0,habitDICT),get_habit(child1,habitDICT)
				if habit0!=None and habit1!=None and habit0!=habit1:
					#for two taxa pairs:
					if set(get_front_names(node))<= set(TAXA):
						label0,label1 = child0.label,child1.label
						print label0,label1
						if label0 in labeled or label1 in labeled: continue
						if habit0 == "W" and habit1 == "H":
							child0.label += " #1"
							child1.label += " #2"
							Wtips,Htips = len(names0),len(names1)
						else:
							child0.label += " #2"
							child1.label += " #1"
							Htips,Wtips = len(names0),len(names1)
						labeled.append(label0)
						labeled.append(label1)
						count += 1
						codeml_treefile = treDIR+cladeID+"."+contrastname+".clade"+str(count)+".W"+str(Wtips)+"H"+str(Htips)
						node,newroot = remove_kink(root,root) #unroot
						with open(codeml_treefile,"w") as outfile:
							outfile.write(remove_branch_length(newick3.tostring(newroot))+";\n")
						codeml_alnfile = alnDIR+cladeID+".pal2nal.trim.paml"
						print label0,label1
						codeml_outfile = codeml_treefile+".codeml.out"
						run_codeml(codeml_treefile,codeml_alnfile,codeml_outfile)
						going = True
						break


