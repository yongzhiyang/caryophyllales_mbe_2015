"""
Find one representative arabidopsis locus id for each mm tree
"""

import newick3,phylo3,os,sys
import collections

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python get_arabidopsis_locus_id_list_for_inclade_list.py inclades_Arabidopsis_locusID cladelist"
		sys.exit(0)

	DICT = {} #key is mmID, value is arabidopsis locus ID
	infile = open(sys.argv[1],"r")
	for line in infile:
		spls = (line.strip()).split("\t")
		if len(spls) == 2 and spls[1] != "NULL":
			DICT[spls[0]] = spls[1]
	infile.close()
	
	incladeLIST = []
	with open(sys.argv[2],"r") as infile:
		for line in infile:
			if len(line) > 3: incladeLIST.append(line.strip())
	
	outfile = open(sys.argv[2]+".arabidopsis","w")
	for ID in incladeLIST:
		try:
			arab_locusid = DICT[ID]
			outfile.write(arab_locusid+"\n")
		except: pass
	outfile.close()
