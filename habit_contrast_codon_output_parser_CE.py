"""
prepare a taxon name table in which each line look like:
GJNX	Aizoaceae_Cypselea_humifusum_H

input trees are the cary clades with name fixed
input alignment are the output of pal2nal

label and unrooot the input rooted cary clades
run codeml
summarize codemel output
"""
import newick3,phylo3,os,sys
import collections

def get_name(label):
	return label[:4]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

#given a codeml string, return the woody and the herbaceous seq ids
#one woody and one herbaceous only
def parse_node_labels_tips(codeml_string):
	spls = codeml_string.split("(")
	for i in spls:
		if "#1" in i:
			out = i
			break
	out = out[:out.find(")")]
	spls = out.split(",")
	if "#1" in spls[0] and "#2" in spls[1]:
		return (spls[0].split("#")[0]).strip(),(spls[1].split("#")[0]).strip()
	elif "#2" in spls[0] and "#1" in spls[1]:
		return (spls[1].split("#")[0]).strip(),(spls[0].split("#")[0]).strip()
	else:
		print "Check tree label format"
		print codeml_string
		exit()

#value of (herbaceous - woody ) / min(woody,herbaceous)
def get_contrast(W,H):
	if W <= 0.0 or H <= 0.0:
		return "NULL"
	return (H-W)/min(W,H)
	
#given a codeml outfile, return the woody and the herbaceous seq ids
#one woody and one herbaceous only
#return contrastdN,contrastdS,contrastdN/dS,WdN,WdS,Womega,HdN,HdS,HdN/dS,and taxa
def parse_codeml_output_tips(codeml_outfile,Wseqid,Hseqid):
	lines = [line.strip() for line in open(codeml_outfile)]
	if "Time used" not in lines[-1] and "Time used" not in lines[-2]:
		print "Unfinished run"
		return None #runs didn't finish
	for i in range(len(lines)):
		line = lines[i]
		if "w (dN/dS) for branches" in line:
			spls = line.split(' ')
			Womega,Homega = float(spls[-2]),float(spls[-1])
		elif "dS tree:" in line:
			tree = newick3.parse(lines[i+1])
			for node in tree.iternodes():
				if node.label == Wseqid: WdS = node.length
				elif node.label == Hseqid: HdS = node.length
		elif "dN tree:" in line:
			tree = newick3.parse(lines[i+1])
			for node in tree.iternodes():
				if node.label == Wseqid: WdN = node.length
				elif node.label == Hseqid: HdN = node.length
	return get_contrast(WdN,HdN),get_contrast(WdS,HdS),WdN,WdS,Womega,HdN,HdS,Homega
	
	
if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python habit_contrast_codon.py codemlDIR outfile"
		sys.exit()
	
	DIR = sys.argv[1]+"/"
	outname = sys.argv[2]
	outfile = open(outname,"w")
	outfile.write("cladeID\tcontrastdN\tcontrastdS\tWdN\tWdS\tWomega\tHdN\tHdS\tHomega\n")
	contrastdNout,contrastdSout,Womegaout,Homegaout = [],[],[],[] #write out summary for plotting in R
	count = 0
	for i in os.listdir(DIR):
		if i[-4:] != ".out": continue
		cladeID = i.replace(".codeml.out","")
		print cladeID
		with open(DIR+cladeID,"r")as infile:
			Wseqid,Hseqid = parse_node_labels_tips(infile.readline())
		out = parse_codeml_output_tips(DIR+i,Wseqid,Hseqid)
		if out != None and count < 3000:
			count += 1
			contrastdN,contrastdS,WdN,WdS,Womega,HdN,HdS,Homega = parse_codeml_output_tips(DIR+i,Wseqid,Hseqid)
			if contrastdN > -5 and contrastdN < 10.: contrastdNout.append(contrastdN)
			if contrastdS > -5 and contrastdS < 10.: contrastdSout.append(contrastdS)
			if Womega >= 0 and Womega < 1.: Womegaout.append(Womega)
			if Homega >= 0 and Homega < 1.: Homegaout.append(Homega)
			outfile.write(cladeID+"\t"+str(contrastdN)+"\t"+str(contrastdS)+"\t")
			outfile.write(str(WdN)+"\t"+str(WdS)+"\t"+str(Womega)+"\t")
			outfile.write(str(HdN)+"\t"+str(HdS)+"\t"+str(Homega)+"\n")
	outfile.close()

	#write out filtered and sorted output for plotting
	with open(outname+"_contrast_dN","w") as outfile:
		for i in sorted(contrastdNout):
			outfile.write(str(i)+"\n")
	with open(outname+"_contrast_dS","w") as outfile:
		for i in sorted(contrastdSout):
			outfile.write(str(i)+"\n")
	with open(outname+"_Womega","w") as outfile:
		for i in sorted(Womegaout):
			outfile.write(str(i)+"\n")
	with open(outname+"_Homega","w") as outfile:
		for i in sorted(Homegaout):
			outfile.write(str(i)+"\n")
