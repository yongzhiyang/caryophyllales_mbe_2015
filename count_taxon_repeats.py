"""
Iterate though all newick files that end with TREE_FILE_IDENTIFIER
Output total number of tips, number of taxa, and how many times each taxon ID
occurs on each tree
"""

import phylo3,newick3,os,sys

CARY = ["BERS","BJKT","EDIT","GJNX","HZTS","OMYK","AAXJ","BWRK","CBJR","CUTE",
		"EYRD","FVXD","HDSY","OHKC","ONLQ","PDQH","SMMC","WGET","WMLW","XSSD",
		"ZBPY","CTYH","CPKP","JLOV","FZQN","OLES","RXEN","SHEZ","SKNL","TJES",
		"WPYJ","YNFJ","KJAA","NXTS","RNBN","SCAO","WQUF","EGOS","HMFE","JAFJ",
		"JGAB","VJPU","ZBTA","AZBL","RUUB","BKQU","MRKX","SFKQ","CGGO","WOBD",
		"FYSJ","BLWH","CPLT","EZGR","GCYL","IWIS","KDCH","LLQV","UQCB","GIWN",
		"CVDF","LKKX","ILU1","ILU2","ILU3","ILU4","ILU5","ILU6","BVGI"]

TREE_FILE_IDENTIFIER = ".cary"

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]
	
if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python count_taxon_repeats.py cary_clade_DIR outFILE"
		sys.exit(0)

	#score taxon counts from all inclades and write to a summary file
	DIR = sys.argv[1]+"/"
	max_count = 0 #keep track of the highest taxon repeats in any gene
	outfile = open(sys.argv[2],"w")
	outfile.write("cladeID\tnum_tips\tnum_taxa\ttaxon_counts\n")
	for treefile in os.listdir(DIR):
		if treefile[-len(TREE_FILE_IDENTIFIER):] not in treefile:
			continue
		print treefile
		with open(DIR+treefile,"r") as infile:
			 intree = newick3.parse(infile.readline())#one tree in each file
		names = get_front_names(intree)
		nameDICT = {} #key is name, value is count
		for name in names:
			if name not in nameDICT:
				nameDICT[name] = 1
			else: nameDICT[name] += 1
		outfile.write(treefile+"\t"+str(len(names))+"\t"+str(len(nameDICT))+"\t")
		for name in nameDICT:
			count = nameDICT[name]
			outfile.write(name+":"+str(count)+",")
			max_count = max(max_count,count)
		outfile.write("\n")
	outfile.close()

	print "Highest taxon repeat in any gene is",max_count
	
