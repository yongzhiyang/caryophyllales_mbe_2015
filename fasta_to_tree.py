"""
cd into the working dir

Change the raxml command (RAXML_CMD) depend on which flavor of raxml and where the
executable is in your machine

Input: unaligned fasta files end with ".fa" in the working dir

alignment: use mafft only if <1000 sequences; use pasta if >= 1000 sequences
tree inference: use raxml if <1000 sequences; use fasttree if >= 1000 sequences

This wrapper replaces the mafft, pasta, phyutility, raxml and fasttree wrapper
"""

import os,sys
from Bio import SeqIO

#RAXML_CMD = "raxmlHPC-PTHREADS-SSE3"
RAXML_CMD = "raxmlHPC-PTHREADS-SSE3-icc"
MIN_CHR = 10 #remove seq shorter than this in trimmed alignments
DIR = "./"
NUM_SEQ_CUTOFF = 1000

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python fasta_to_tree.py number_cores dna/aa"
		sys.exit(0)
	
	num_cores = sys.argv[1]
	seqtype = sys.argv[2]
	if seqtype == "aa":
		mafft_seqtype = "--amino"
		pasta_seqtype = "Protein"
		phyutility_seqtype = " -aa"
		raxmlmodel = "PROTCATWAG"
		fasttreemodel = "-wag"
	elif seqtype == "dna":
		mafft_seqtype = "--nuc"
		pasta_seqtype = "DNA"
		phyutility_seqtype = ""
		raxmlmodel = "GTRCAT"
		fasttreemodel = "-nt -gtr"
	else:
		print "Input data type: dna or aa"
		sys.exit()
	DIR = "./"
	
	done = [] #record existing files in the dir that are not input
	for i in os.listdir(DIR):
		if i[-3:] != ".fa":
			done.append(i)
	
	filecount = 0
	for i in os.listdir(DIR):
		if i[-3:] != ".fa": continue #only process fasta files
		clusterID = i.split(".")[0]
		filecount += 1
		
		#get some basic stats for the fasta file
		seqcount = 0 #record how many sequences in the fasta file
		maxlen = 0 #record the longest seq length
		seqlen = 0 #record the current seq length
		with open(DIR+i,"r") as infile:
			for line in infile:
				if line[0] == ">":
					seqcount += 1
					maxlen = max(seqlen,maxlen)
					seqlen = 0
				else: seqlen += len((line.strip()).replace("-",""))
		if seqcount <= 4:
			print "Four or less sequences"
			continue
		else:
			print i,seqcount,"sequences, longest sequence has",maxlen,"characters"
		
		#align
		if seqcount>=NUM_SEQ_CUTOFF or (maxlen>=10000 and seqtype=="aa") or (maxlen>=100000 and seqtype=="dna"):
			alignment = i+".pasta.aln"
		else: alignment = i+".mafft.aln"
		if alignment not in done:
			if "pasta" in alignment:
				#pasta does not recognize "*" or "U"
				handle = open(DIR+i,"r")
				outfile = open(DIR+i+".temp","w")
				for record in SeqIO.parse(handle,"fasta"):
					seqid,seq = str(record.id),str(record.seq)
					#remove U which is usually not in aa alphabet
					seq = (seq.replace("U","X")).replace("u","x")
					#remove stop codon and seq after it since sate crashes at stop codon
					seq = seq[:seq.find("*")]
					outfile.write(">"+seqid+"\n"+seq+"\n")
				handle.close()
				outfile.close()
				cmd = "run_pasta.py --input="+DIR+i+".temp --datatype="+pasta_seqtype
				print cmd
				os.system(cmd)
				os.system("mv "+DIR+"pastajob.marker001."+i+".temp.aln "+alignment)
				#remove intermediate files
				os.system("rm "+DIR+"pastajob*")
				os.system("rm "+DIR+i+".temp")
			else:
				com = "mafft --genafpair --maxiterate 1000 "+mafft_seqtype+" --thread "+num_cores+" "
				#com += "--anysymbol " #use this when there are "U"s in aa sequences
				com += DIR+i+" > "+alignment
				print com
				os.system(com)
			print "Alignment written to",alignment
		
		#trim alignment
		trimmed = alignment+"-cln"
		if trimmed not in done:
			if seqcount >= NUM_SEQ_CUTOFF:
				min_col_occup = 0.01 #MIN_COLUMN_OCCUPANCY, for sparse matrices
			else: min_col_occup = 0.1 #for smaller matrices
			cmd = "phyutility"+phyutility_seqtype+" -clean "+str(min_col_occup)
			cmd += " -in "+alignment+" -out "+alignment+"-pht"
			print cmd
			os.system(cmd)			
			#remove empty and very short seqs	
			handle = open(alignment+"-pht","r")
			outfile = open(trimmed,"w")
			for record in SeqIO.parse(handle,"fasta"):
				seqid,seq = str(record.id),str(record.seq)
				if len(seq.replace("-","")) >= MIN_CHR:
					outfile.write(">"+seqid+"\n"+seq+"\n")
			handle.close()
			outfile.close()
			#remove intermediate file
			os.system("rm "+alignment+"-pht")
			print "Trimmed alignment written to",trimmed

		#tree inference
		if clusterID+".fasttree" not in done and clusterID+".raxml" not in done:
			if seqcount < NUM_SEQ_CUTOFF:
				cmd = RAXML_CMD+" -T "+num_cores+" -p 12345 -s "+trimmed+" -n "
				cmd += trimmed+" -m "+raxmlmodel
				print cmd
				os.system(cmd)
				cmd = "mv RAxML_bestTree."+trimmed+" "+clusterID+".raxml"
				os.system(cmd)
				os.system("rm RAxML*"+trimmed) #remove raxml output files
			else: 	
				cmd = "FastTreeMP "+fasttreemodel+" -quiet "+trimmed+" >"+clusterID+".fasttree"
				print cmd
				os.system(cmd)
	
	if filecount == 0:
		print "No file end with .fa found in the current directory"
	os.system("rm *reduced") #remove raxml output files
